import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { Page } from "ui/page";
import { FirebaseService } from "../shared/service";

@Component({
    selector: "ResetPassword",
    moduleId: module.id,
    templateUrl: "./reset-password.component.html"
})
export class ResetPasswordComponent implements OnInit {

    input: any;
    // tslint:disable-next-line:max-line-length
    constructor(private page: Page, private routerExtension: RouterExtensions, private firebaseService: FirebaseService) {
        this.input = {
            email: ""
        };
    }

    // tslint:disable-next-line:no-empty
    ngOnInit(): void { }

    login() {
        this.routerExtension.navigate(["/login"]);
    }

    reset() {
        if (this.input.email === "") {
            alert("Email Required");
        } else {
            this.firebaseService.resetPassword(this.input);
        }
    }
}
