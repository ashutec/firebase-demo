import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoginComponent } from "./login";
import { ResetPasswordComponent } from "./reset-password";
import { AuthGuard } from "./shared/service";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full", canActivate: [AuthGuard] },
    { path: "login", component: LoginComponent },
    { path: "resetPassword", component: ResetPasswordComponent },
    { path: "home", loadChildren: "./home#HomeModule", canActivate: [AuthGuard] },
    { path: "todo", loadChildren: "./todo#TodoModule", canActivate: [AuthGuard] },
    { path: "changePassword", loadChildren: "./change-password#ChangePasswordModule", canActivate: [AuthGuard] }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule],
})
export class AppRoutingModule { }
