import { Component, OnInit } from "@angular/core";
import * as firebase from "nativescript-plugin-firebase";
import { BackendService } from "./shared/service/backend.service";
import { FirebaseService } from "./shared/service/firebase.service";
import { StorageService } from "./shared/service/storage.service";

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {
    constructor(
        private firebaseService: FirebaseService,
        private storageService: StorageService
    ) {
        this.getFireBaseToken();
        this.getFireBaseMessage();
    }

    // tslint:disable-next-line:no-empty
    ngOnInit() {
    }

    getFireBaseToken() {
        firebase.getCurrentPushToken()
            .then((token) => {
                console.log("++++++Get FireBase Token :::", token);
                setTimeout(() => {
                    this.storageService.set("firebaseToken", token);
                    this.firebaseService.setToken(token);
                }, 2000);
            });
    }

    getFireBaseMessage() {
        firebase.addOnMessageReceivedCallback((onMessageReceived) => {
            // console.log("==========================", JSON.stringify(onMessageReceived));
            alert(JSON.stringify(onMessageReceived.body));

        });
    }
}
