import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-pro-ui/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-pro-ui/sidedrawer/angular";
import { Page } from "ui/page";
import { FirebaseService, StorageService } from "../shared/service";

@Component({
    selector: "changePassword",
    moduleId: module.id,
    templateUrl: "./change-password.component.html"
})
export class ChangePasswordComponent implements OnInit {

    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    input: any;
    isAuthenticating = false;
    isLoggingIn = true;

    private _sideDrawerTransition: DrawerTransitionBase;

    // tslint:disable-next-line:max-line-length
    constructor(private page: Page,
                private routerExtension: RouterExtensions,
                private firebaseService: FirebaseService,
                private storageService: StorageService) {
        this.input = {
            email: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: ""
        };
    }

    ngOnInit() {
        const message = "Change Password Component";
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.input.email = this.storageService.get("email");
        this.input.oldPassword = this.storageService.get("password");
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    /* ***********************************************************
    * According to guidelines, if you have a drawer on your page, you should always
    * have a button that opens it. Use the showDrawer() function to open the app drawer section.
    *************************************************************/
    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }

    changePassword(args) {
        if (this.input.newPassword !== this.input.confirmPassword) {
            alert("Password are not match");
        } else {
            this.firebaseService.reAuthenticate(this.input.email, this.input.oldPassword)
                .then(() => {
                    // you can now safely  change the password
                    this.firebaseService.changePassword(this.input);
                }, (error) => {
                    alert("Re-authenticated Error");
                });
        }
    }
}
