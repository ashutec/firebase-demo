import { Component, OnInit, ViewChild } from "@angular/core";
import { enableLocationRequest, getCurrentLocation, isEnabled, Location } from "nativescript-geolocation";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-pro-ui/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-pro-ui/sidedrawer/angular";

import * as firebase from "nativescript-plugin-firebase";
import { BackendService, FirebaseService, StorageService } from "../shared/service";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
    /* ***********************************************************
    * Use the @ViewChild decorator to get a reference to the drawer component.
    * It is used in the "onDrawerButtonTap" function below to manipulate the drawer.
    *************************************************************/
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;
    fireBaseToken = "1";
    time = new Date();
    location = { latitude: 0, longitude: 0 };

    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(
        private firebaseService: FirebaseService,
        private storageService: StorageService
    ) {
        // Home component constructor
    }

    ngOnInit(): void {
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.fireBaseToken = this.storageService.get("firebaseToken");
        this.firebaseService.getToken().subscribe((token) => {
            this.fireBaseToken = token;
        });
        this.saveLocation();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    /* ***********************************************************
    * According to guidelines, if you have a drawer on your page, you should always
    * have a button that opens it. Use the showDrawer() function to open the app drawer section.
    *************************************************************/
    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }

    saveLocation() {
        try {
            this.getCurrentPosition().then((loc) => {
                console.log("saveLocation" + JSON.stringify(loc));
                this.location = loc;
                const location = {
                    latitude: loc.latitude, // 23.044708,
                    longitude: loc.longitude, // 72.514368,
                    altitude: loc.altitude, // 0,
                    horizontalAccuracy: loc.horizontalAccuracy, //  5,
                    verticalAccuracy: loc.verticalAccuracy, //  -1,
                    speed: loc.speed, // -1,
                    direction: loc.direction, // -1,
                    timestamp: JSON.parse(JSON.stringify(loc.timestamp)) //"2018-01-11T07:31:27.726Z"
                };

                firebase.push("/location",
                    { location, user_UID: BackendService.token, time: Date.now() })
                    .then(() => {
                        alert("success");
                        // this.getLocation();
                    });
            });
        } catch (error) {
            console.log("saveLocation :" + (error.message || error));
        }

    }

    getCurrentPosition() {
        try {
            return this.enableLocationPermission()
                .then((enabled) => {

                    return getCurrentLocation({
                        desiredAccuracy: 3, updateDistance: 10,
                        maximumAge: 20000, timeout: 20000
                    });
                });
        } catch (error) {
            console.log("ErrorLocation: " + (error.message || error));
        }
    }

    enableLocationPermission() {
        return isEnabled()
            .then((enabled) => {
                return !enabled ? enableLocationRequest() : <any>true;
            });
        //  .catch((error) => { console.log("Error12: " + JSON.stringify(error.message || error)); });

    }

    getLocation() {
        firebase.getValue("/location").then((result) => {
            // console.log("result is" + typeof result);
        });
    }

}
