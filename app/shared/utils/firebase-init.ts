import { setString } from "application-settings";
import * as firebase from "nativescript-plugin-firebase";

export function  fireBaseInit() {
    firebase.init({
        iOSEmulatorFlush: true,
        onPushTokenReceivedCallback: (token) => {
            setString("firebaseToken", token);
        },
        // persist should be set to false as otherwise numbers aren't returned during livesync
        persist: true
    }).then((instance) =>
        console.log("firebase.init done"))
        .catch((error) =>
            console.log("firebase.init error: " + error));
}
