import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { isAndroid, isIOS } from "platform";
import "rxjs/add/operator/share";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
import { BackendService } from "./backend.service";
import { StorageService } from "./storage.service";
declare const com: any;
declare const android: any;
declare const FIRCrashLog: any;
declare const FIRCrashNSLog: any;

@Injectable()
export class FirebaseService {

    private tokenSubject: Subject<{}> = new Subject<{}>();

    constructor(
        private router: RouterExtensions,
        private storageService: StorageService

    ) { }

    setToken(token): void {
        this.tokenSubject.next(token);
    }

    getToken(): Observable<any> {
        return this.tokenSubject.asObservable();
    }

    login(user) {
        const userInput = {
            email: user.email,
            password: user.password
        };

        return firebase.login({
            type: firebase.LoginType.PASSWORD,
            passwordOptions: userInput
        }).then((result) => {
            BackendService.token = result.uid;

            return JSON.stringify(result);
        }).catch((errorMessage) => {
            alert(errorMessage);
        });
    }

    logout() {
        this.storageService.remove("token");
        this.storageService.remove("email");
        this.storageService.remove("password");
        BackendService.token = "";
        firebase.logout();
        this.router.navigate(["/login"], { clearHistory: true });
    }

    resetPassword(input) {
        const userInput = {
            email: input.email
        };
        firebase.resetPassword(userInput).then(() => {
            // called when password reset was successful,
            // you could now prompt the user to check his email
            alert("Check email for reset");
            this.router.navigate(["/login"], { clearHistory: true });
        }).catch((errorMessage) => {
            console.log(errorMessage);
        });
    }

    changePassword(input) {
        const userInput = {
            email: input.email,
            oldPassword: input.oldPassword,
            newPassword: input.newPassword
        };
        firebase.changePassword(userInput).then(() => {
            // called when password change was successful
            alert("Password Change Successfully");
            this.logout();
        }).catch((errorMessage) => {
            alert(errorMessage);
            console.log(errorMessage);
        });
    }

    reAuthenticate(email, password) {
        const userInput = {
            email,
            password
        };

        return firebase.reauthenticate({
            type: firebase.LoginType.PASSWORD, // or GOOGLE / FACEBOOK
            passwordOptions: userInput
        });
    }

    enableFeatured() {
        return firebase.getRemoteConfig({
            developerMode: true,
            cacheExpirationSeconds: 10,
            properties: [{
                key: "enable_featured",
                default: false
            }]
        });
    }

    // used to send crash report to firebase
    sendCrashLog(arg) {
        return new Promise((resolve, reject) => {
            try {
                if (isAndroid) {
                    if (typeof (com.google.firebase.crash) === "undefined") {
                        reject("Make sure firebase-crash is in the plugin's include.gradle");

                        return;
                    }
                    if (!arg.message) {
                        reject("The mandatory 'message' argument is missing");

                        return;
                    }
                    const exception = new android.util.AndroidException(arg.message);
                    com.google.firebase.crash.FirebaseCrash.report(exception);
                    resolve();
                }
                if (isIOS) {

                    if (typeof (FIRCrashLog) === "undefined") {
                        reject(`Make sure 'Firebase/Crash' is in the plugin's Podfile - and if
                           it is there's currently a problem with this Pod which is outside out span of control :(")`);

                        return;
                    }
                    if (!arg.message) {
                        reject("The mandatory 'message' argument is missing");

                        return;
                    }
                    if (arg.showInConsole) {
                        FIRCrashNSLog(arg.message);
                    } else {
                        FIRCrashLog(arg.message);
                    }
                    resolve();
                }
            } catch (ex) {
                console.log("Error in firebase.sendCrashLog: " + ex);
                reject(ex);
            }
        });
    }

}
