import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedModule } from "../shared";
import { ChangePasswordComponent } from "./change-password.component";
import { ChangePasswordRoutingModule } from "./change-password.routing.module";

@NgModule({
    imports: [
        ChangePasswordRoutingModule,
        SharedModule
    ],
    declarations: [
        ChangePasswordComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ChangePasswordModule { }
