import { Component, Input, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { Observable } from "rxjs/Observable";
import { FirebaseService } from "../../shared/service/firebase.service";
import { BackendService } from "../service/backend.service";

const appSettings = require("application-settings");
/* ***********************************************************
* Keep data that is displayed in your app drawer in the MyDrawer component class.
* Add new data objects that you want to display in the drawer here in the form of properties.
*************************************************************/
@Component({
    selector: "MyDrawer",
    moduleId: module.id,
    templateUrl: "./my-drawer.component.html",
    styleUrls: ["./my-drawer.component.css"]
})
export class MyDrawerComponent implements OnInit {
    /* ***********************************************************
    * The "selectedPage" is a component input property.
    * It is used to pass the current page title from the containing page component.
    * You can check how it is used in the "isPageSelected" function below.
    *************************************************************/

    email;
    isTodo;
    @Input() selectedPage: string;

    // tslint:disable-next-line:max-line-length
    constructor(
        private router: RouterExtensions,
        private _activatedRoute: ActivatedRoute,
        private firebaseService: FirebaseService
    ) {
        this.email = appSettings.getString("email");
        setTimeout(() => {
            this.firebaseService.enableFeatured()
                .then((result: any) => {
                    this.isTodo = result.properties.enable_featured;
                });
        }, 5);
    }
    ngOnInit() {
        /* ***********************************************************
        * Use the MyDrawerComponent "onInit" event handler to initialize the properties data values.
        *************************************************************/
    }

    /* ***********************************************************
    * The "isPageSelected" function is bound to every navigation item on the <MyDrawerItem>.
    * It is used to determine whether the item should have the "selected" class.
    * The "selected" class changes the styles of the item, so that you know which page you are on.
    *************************************************************/
    isPageSelected(pageTitle: string): boolean {
        return pageTitle === this.selectedPage;
    }
}
