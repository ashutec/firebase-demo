import { Component, OnInit, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { DrawerTransitionBase, SlideInOnTopTransition } from "nativescript-pro-ui/sidedrawer";
import { RadSideDrawerComponent } from "nativescript-pro-ui/sidedrawer/angular";
import { Page } from "ui/page";
import { FirebaseService, FireStoreService, StorageService } from "../shared/service";
import { GenerateUniqNumber } from "../shared/utils";

@Component({
    selector: "Todo",
    moduleId: module.id,
    templateUrl: "./todo.component.html"
})
export class TodoComponent implements OnInit {
    /* ***********************************************************
    * Use the @ViewChild decorator to get a reference to the drawer component.
    * It is used in the "onDrawerButtonTap" function below to manipulate the drawer.
    *************************************************************/
    @ViewChild("drawer") drawerComponent: RadSideDrawerComponent;

    input: any;
    toDoListArray = [];
    email;
    isEmpty;
    private _sideDrawerTransition: DrawerTransitionBase;


    constructor(
        private page: Page,
        private routerExtension: RouterExtensions,
        private firebaseService: FirebaseService,
        private storageService: StorageService,
        private utilityService: FireStoreService) {
        this.input = {
            toDo: ""
        };

    }

    /* ***********************************************************
    * Use the sideDrawerTransition property to change the open/close animation of the drawer.
    *************************************************************/
    ngOnInit(): void {
        this.isEmpty = false;
        this.email = this.storageService.get("email");
        this.utilityService.getTodoList().then((result) => this.toDoListArray = result);
        this._sideDrawerTransition = new SlideInOnTopTransition();
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    addToDo() {
        const randomNumber = GenerateUniqNumber.generateUniqId();
        this.toDoListArray.push({
            name: this.input.toDo,
            userName: this.email,
            _id: randomNumber
        });
        this.utilityService.insertTodoList(this.input.toDo, this.email, randomNumber);
        this.input.toDo = "";
    }


    onItemTap(item, index) {
        this.toDoListArray.splice(index, 1);
        this.utilityService.deleteTodoListById(item._id);
    }

    textChange(args) {
        this.isEmpty = (args.length === 0) ? false : true;
    }

    /* ***********************************************************
    * According to guidelines, if you have a drawer on your page, you should always
    * have a button that opens it. Use the showDrawer() function to open the app drawer section.
    *************************************************************/
    onDrawerButtonTap(): void {
        this.drawerComponent.sideDrawer.showDrawer();
    }
}
