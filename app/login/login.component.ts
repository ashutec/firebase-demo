import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";
import { isAndroid } from "platform";
import { Observable } from "rxjs/Observable";
import { Page } from "ui/page";
import { FirebaseService , StorageService } from "../shared/service";

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html"
})
export class LoginComponent implements OnInit {

    input: any;
    isAuthenticating = false;
    isLoggingIn = true;
    message$: Observable<any>;

    constructor(
        private page: Page,
        private routerExtension: RouterExtensions,
        private firebaseService: FirebaseService,
        private storageService: StorageService
    ) {
        this.routerExtension.navigate(["/login"], { clearHistory: true });
        page.actionBarHidden = true;
        this.input = {
            email: "",
            password: ""
        };
    }

    // tslint:disable-next-line:no-empty
    ngOnInit(): void {
    }

    login(args) {
        this.isAuthenticating = true;
        if (this.isLoggingIn) {
            this.firebaseService.login(this.input)
                .then(() => {
                    this.isAuthenticating = false;
                    this.storageService.set("email", this.input.email);
                    this.storageService.set("password", this.input.password);
                    // this.storageService.set("uuid", this.input.uuid);

                    this.routerExtension.navigate(["/home"], { clearHistory: true });

                })
                .catch((message: any) => {
                    // firebase send crash log will send crash log to firebase
                    this.firebaseService.sendCrashLog({ message, showInConsole: true });
                    alert("Email Or Password Not Correct");
                    this.isAuthenticating = false;
                });
        }
    }

    reset() {
        this.routerExtension.navigate(["/resetPassword"]);
    }
}
