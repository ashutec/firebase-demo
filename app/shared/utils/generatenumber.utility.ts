export class GenerateUniqNumber {

    static generateUniqId() {
        return this.randomNumber() + this.randomNumber() +
            this.randomNumber() + this.randomNumber() +
            this.randomNumber() + this.randomNumber() +
            this.randomNumber() + this.randomNumber();
    }
    static randomNumber() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

}

