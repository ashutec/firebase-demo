import { Injectable } from "@angular/core";
import { getString, remove, setString } from "application-settings";

@Injectable()
export class StorageService {

    set(key, value) {
        setString(key, value);
    }

    get(key): string {

        return getString(key);
    }

    remove(key) {
        remove(key);
    }

}
