import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedModule } from "../shared";
import { TodoComponent } from "./todo.component";
import { TodoRoutingModule } from "./todo.routing.module";

@NgModule({
    imports: [
        TodoRoutingModule,
        SharedModule
    ],
    declarations: [
        TodoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TodoModule { }
