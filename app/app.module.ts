import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NSModuleFactoryLoader } from "nativescript-angular/router";
import { AppRoutingModule } from "./app.routing.module";
import { SharedModule } from "./shared";

import { AppComponent } from "./app.component";
import { HomeModule } from "./home";
import { LoginComponent } from "./login";
import { ResetPasswordComponent } from "./reset-password";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        SharedModule,
        AppRoutingModule,
        NativeScriptModule,
        HomeModule
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        ResetPasswordComponent
    ],
    providers: [
        { provide: NgModuleFactoryLoader, useClass: NSModuleFactoryLoader }
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
