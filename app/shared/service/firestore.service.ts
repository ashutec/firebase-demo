import { Injectable } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import * as firebase from "nativescript-plugin-firebase";

@Injectable()
export class FireStoreService {

    constructor() { }

    getTodoList() {
        const toDoListArray = [];
        const todoListCollection = firebase.firestore.collection("todolists");
        const query = todoListCollection.where("isDeleted", "==", false);

        return query.get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    toDoListArray.push({
                        name: doc.data().name,
                        userName: doc.data().userName,
                        _id: doc.data()._id
                    });
                });

                return toDoListArray;
            });
    }

    insertTodoList(todoName, email, randomNumber) {
        const todoInput = {
            name: todoName,
            userName: email,
            isDeleted: false,
            _id: randomNumber
        };
        const todoListCollection = firebase.firestore.collection("todolists");
        todoListCollection.doc(randomNumber).set(todoInput);
    }

    deleteTodoListById(docId) {
        const deleteTodoList = firebase.firestore.collection("todolists").doc(docId);
        deleteTodoList.update({
            isDeleted: true
        });
    }
}
